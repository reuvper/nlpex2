###################################
# NLP - Exercise 2:
# Reuven Peretz - 201384310
# Inon Kaplan - 300460094
###################################

from nltk.corpus import brown
from nltk.metrics import ConfusionMatrix
import math
import re


# Question 2.a:
tagged_sents = brown.tagged_sents(categories='news')
training_set = tagged_sents[: math.floor(len(tagged_sents) * .9)]
test_set = tagged_sents[math.floor(len(tagged_sents) * .9):]


# Question 2.b.i:
def preprocessing(training_set):

    # preprocessing
    words_to_tags = dict()
    tags_to_words = dict()
    preceing_tag_count = dict()
    preceing_tag_count['**'] = dict()
    tag_occurrences = dict()
    word_occurrences = dict()

    for sentence in training_set:
        i = 0
        for tagged_word in sentence:
            preceding_tag = '*'
            if (i > 0):
                preceding_tag = sentence[i - 1][1]

            if (not tagged_word[1] in preceing_tag_count):
                preceing_tag_count[tagged_word[1]] = dict()
            if (not preceding_tag in preceing_tag_count[tagged_word[1]]):
                preceing_tag_count[tagged_word[1]][preceding_tag] = 1
            else:
                preceing_tag_count[tagged_word[1]][preceding_tag] += 1
            if (i == len(sentence) - 1):
                if (not tagged_word[1] in preceing_tag_count['**']):
                    preceing_tag_count['**'][tagged_word[1]] = 1
                else:
                    preceing_tag_count['**'][tagged_word[1]] += 1

            if (not tagged_word[0] in words_to_tags):
                words_to_tags[tagged_word[0]] = dict()
            if (not tagged_word[1] in words_to_tags[tagged_word[0]]):
                words_to_tags[tagged_word[0]][tagged_word[1]] = 1
            else:
                words_to_tags[tagged_word[0]][tagged_word[1]] += 1

            if (not tagged_word[1] in tags_to_words):
                tags_to_words[tagged_word[1]] = dict()
            if (not tagged_word[0] in tags_to_words[tagged_word[1]]):
                tags_to_words[tagged_word[1]][tagged_word[0]] = 1
            else:
                tags_to_words[tagged_word[1]][tagged_word[0]] += 1
            i += 1

    for tag in tags_to_words:
        sum = 0
        for word in tags_to_words[tag]:
            sum += tags_to_words[tag][word]
        tag_occurrences[tag] = sum
    tag_occurrences['*'] = len(training_set)
    tag_occurrences['**'] = tag_occurrences['*']

    for word in words_to_tags:
        sum = 0
        for tag in words_to_tags[word]:
            sum += words_to_tags[word][tag]
        word_occurrences[word] = sum

    return [words_to_tags, tags_to_words, preceing_tag_count, tag_occurrences, word_occurrences];


preprocessing_results = preprocessing(training_set)
words_to_tags = preprocessing_results[0]
tags_to_words = preprocessing_results[1]
preceing_tag_count = preprocessing_results[2]
tag_occurrences = preprocessing_results[3]
word_occurrences = preprocessing_results[4]


# Question 2.b.ii:
test_words_to_tags = dict()
num_test_words = 0
num_correct_tags = 0
num_unkown_correct_tags = 0
num_known_correct_tags = 0
for sentence in test_set:
    for tagged_word in sentence:
        num_test_words += 1
        if (not tagged_word[0] in words_to_tags):
            test_words_to_tags[tagged_word[0]] = 'NN'
            if (tagged_word[1] == 'NN'):
                num_unkown_correct_tags += 1
        else:
            max_occur = 0
            for tag in words_to_tags[tagged_word[0]]:
                if (words_to_tags[tagged_word[0]][tag] > max_occur):
                    max_occur = words_to_tags[tagged_word[0]][tag]
                    test_words_to_tags[tagged_word[0]] = tag
            if (test_words_to_tags[tagged_word[0]] == tagged_word[1]):
                num_known_correct_tags += 1

unknown_success_rate = num_unkown_correct_tags / num_test_words
known_success_rate = num_known_correct_tags / num_test_words
total_success_rate = unknown_success_rate + known_success_rate

print("************* b.ii ***************")
print("unknown error rate")
print(1 - unknown_success_rate)
print("known error rate")
print(1 - known_success_rate)
print("total error rate")
print(1 - total_success_rate)



# Question 2.C.i:
def get_emission(tag, word, tags_to_words, tag_occurrences):
    return tags_to_words[tag][word] / tag_occurrences[tag];

def get_transition(tag, prev_tag, preceing_tag_count, tag_occurrences):
    return preceing_tag_count[tag][prev_tag] / tag_occurrences[tag];


# Question 2.D.i:
def get_add_one_smoothing_emission(tag, word, tags_to_words, tag_occurrences, vocabulary_size):
    pair_occurrences = 1
    if (word in tags_to_words[tag]):
        pair_occurrences += tags_to_words[tag][word]
    return pair_occurrences / (tag_occurrences[tag] + vocabulary_size);


# Question 2.C.ii:
def viterbi(sentence, preprocessing_results, use_add_one_smoothing=False):

    words_to_tags = preprocessing_results[0]
    tags_to_words = preprocessing_results[1]
    preceing_tag_count = preprocessing_results[2]
    tag_occurrences = preprocessing_results[3]

    pi = dict()
    bp = dict()
    for i in range(len(sentence)):
        pi[i] = dict()
        bp[i] = dict()
        for tag in tags_to_words:
            if (i == 0):
                try:
                    transition = get_transition(tag, '*', preceing_tag_count, tag_occurrences)
                    if (use_add_one_smoothing):
                        emission = get_add_one_smoothing_emission(tag, sentence[0][0], tags_to_words, tag_occurrences, len(words_to_tags))
                    else:
                        emission = get_emission(tag, sentence[0][0], tags_to_words, tag_occurrences)
                    pi[i][tag] = transition * emission
                    bp[i][tag] = "*"
                except KeyError as detail:
                    bp[i][tag] = "*"
                    pi[i][tag] = 0
            else:
                pi[i][tag] = 0
                bp[i][tag] = 'NN'
                for preceding_tag in tags_to_words:
                    try:

                        transition = get_transition(tag, preceding_tag, preceing_tag_count, tag_occurrences)
                        if (use_add_one_smoothing):
                            emission = get_add_one_smoothing_emission(tag, sentence[i][0], tags_to_words,
                                                                      tag_occurrences, len(words_to_tags))
                        else:
                            emission = get_emission(tag, sentence[i][0], tags_to_words, tag_occurrences)

                        curr = pi[i - 1][preceding_tag] * transition * emission
                    except:
                        curr = 0 # probably transition or emission does not exist, thus curr = 0
                    finally:
                        if (curr > pi[i][tag]):
                            pi[i][tag] = curr
                            bp[i][tag] = preceding_tag

    max_tag = 'NN'
    max_prob = 0
    for tag in tags_to_words:
        try:
            transition = get_transition('**', tag, preceing_tag_count, tag_occurrences)
            curr = transition * pi[len(sentence)-1][tag]
            if (curr > max_prob):
                max_prob = curr
                max_tag = tag
        except:
            pass
    tags = list()
    tags.append(max_tag)
    last_tag = max_tag
    for i in range(len(sentence) - 2, -1, -1):
        tags.append(bp[i + 1][last_tag])
        last_tag = bp[i + 1][last_tag]
    return tags[::-1]



num_test_words = 0
num_unkown_correct_tags = 0
num_known_correct_tags = 0
for sentence in test_set:
    predicted_tags = viterbi(sentence, preprocessing_results)
    for i in range(len(sentence)):
        tagged_word = sentence[i]
        num_test_words += 1
        if (not tagged_word[0] in words_to_tags):
            if (predicted_tags[i] == tagged_word[1]):
                num_unkown_correct_tags += 1
        else:
            if (predicted_tags[i] == tagged_word[1]):
                num_known_correct_tags += 1

unknown_success_rate = num_unkown_correct_tags / num_test_words
known_success_rate = num_known_correct_tags / num_test_words
total_success_rate = unknown_success_rate + known_success_rate

print("************* c.iii ***************")
print("unknown error rate")
print(1 - unknown_success_rate)
print("known error rate")
print(1 - known_success_rate)
print("total error rate")
print(1 - total_success_rate)



print("************* d.ii ***************")
num_test_words = 0
num_unkown_correct_tags = 0
num_known_correct_tags = 0
for sentence in test_set:
    predicted_tags = viterbi(sentence, preprocessing_results, use_add_one_smoothing=True)
    for i in range(len(sentence)):
        tagged_word = sentence[i]
        num_test_words += 1
        if (not tagged_word[0] in words_to_tags):
            if (predicted_tags[i] == tagged_word[1]):
                num_unkown_correct_tags += 1
        else:
            if (predicted_tags[i] == tagged_word[1]):
                num_known_correct_tags += 1

unknown_success_rate = num_unkown_correct_tags / num_test_words
known_success_rate = num_known_correct_tags / num_test_words
total_success_rate = unknown_success_rate + known_success_rate
print("unknown error rate")
print(1 - unknown_success_rate)
print("known error rate")
print(1 - known_success_rate)
print("total error rate")
print(1 - total_success_rate)


# Question 2.E.i:
preprocessing_results = preprocessing(training_set)
words_to_tags = preprocessing_results[0]
tags_to_words = preprocessing_results[1]
preceing_tag_count = preprocessing_results[2]
tag_occurrences = preprocessing_results[3]
word_occurrences = preprocessing_results[4]

patterns = [
    (re.compile('[0-9][0-9]$'), 'twoDigitNum'),
    (re.compile('[0-9][0-9][0-9][0-9]$'), 'fourDigitNum'),
    (re.compile('^[0-9]+$'), 'othernum'),
    (re.compile('^[0-9-]+$'), 'containsDigitsAndDash'),
    (re.compile('^[0-9/]+$'), 'containsDigitsAndSlash'),
    (re.compile('^[0-9,]+$'), 'containsDigitsAndComma'),
    (re.compile('^[0-9.]+$'), 'containsDigitsAndPeriod'),
    (re.compile('^[A-Z]+$'), 'allCaps'),
    (re.compile('^[a-z]+$'), 'lowercase'),
    (re.compile('^[A-Z][.]$'), 'capPeriod'),
    (re.compile('^[A-Z][a-z]+$'), 'initCap'),
    (re.compile('^[a-z0-9A-Z]+$'), 'containsDigitsAndAlpha'),
    (re.compile('.*'), 'defaultPseudo') # default
 ]


def get_pseudo_word(word):
    for pattern in patterns:
        if pattern[0].match(word):
            return pattern[1];


word_occurrences = preprocessing(training_set)[4]
threshold = 5;

# Replace low-frequency words in training set sentences with pseudo words.
new_training_set = []
for sentence in training_set:
    for i in range(len(sentence)):
        if word_occurrences[sentence[i][0]] < threshold:
            sentence[i] = (get_pseudo_word(sentence[i][0]), sentence[i][1]);
    new_training_set.append(sentence)

# Compute preprocessing with new training set.
preprocessing_results = preprocessing(new_training_set)
words_to_tags = preprocessing_results[0]
word_occurrences = preprocessing_results[4]

# Replace unknown words in test set sentences with pseudo words.
new_test_set = []
for sentence in test_set:
    for i in range(len(sentence)):
        if (not (sentence[i][0] in word_occurrences)):
            sentence[i] = (get_pseudo_word(sentence[i][0]), sentence[i][1]);
    new_test_set.append(sentence)


# Question 2.E.ii:
print("************* e.ii ***************")
num_test_words = 0
num_unkown_correct_tags = 0
num_known_correct_tags = 0
for sentence in test_set:
    predicted_tags = viterbi(sentence, preprocessing_results)
    for i in range(len(sentence)):
        tagged_word = sentence[i]
        num_test_words += 1
        if (not tagged_word[0] in words_to_tags):
            if (predicted_tags[i] == tagged_word[1]):
                num_unkown_correct_tags += 1
        else:
            if (predicted_tags[i] == tagged_word[1]):
                num_known_correct_tags += 1

unknown_success_rate = num_unkown_correct_tags / num_test_words
known_success_rate = num_known_correct_tags / num_test_words
total_success_rate = unknown_success_rate + known_success_rate
print("unknown error rate")
print(1 - unknown_success_rate)
print("known error rate")
print(1 - known_success_rate)
print("total error rate")
print(1 - total_success_rate)


# Question 2.E.iii:
print("************* e.iii ***************")
num_test_words = 0
num_unkown_correct_tags = 0
num_known_correct_tags = 0
total_true_tags = []
total_predicted_tags = []

for sentence in test_set:
    predicted_tags = viterbi(sentence, preprocessing_results, use_add_one_smoothing=True)
    total_predicted_tags += predicted_tags
    for i in range(len(sentence)):
        total_true_tags.append(sentence[i][1])
        tagged_word = sentence[i]
        num_test_words += 1
        if (not tagged_word[0] in words_to_tags):
            if (predicted_tags[i] == tagged_word[1]):
                num_unkown_correct_tags += 1
        else:
            if (predicted_tags[i] == tagged_word[1]):
                num_known_correct_tags += 1

unknown_success_rate = num_unkown_correct_tags / num_test_words
known_success_rate = num_known_correct_tags / num_test_words
total_success_rate = unknown_success_rate + known_success_rate
print("unknown error rate")
print(1 - unknown_success_rate)
print("known error rate")
print(1 - known_success_rate)
print("total error rate")
print(1 - total_success_rate)

cm = ConfusionMatrix(total_true_tags, total_predicted_tags)
print(cm)
